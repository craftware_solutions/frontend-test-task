# Design requirements: #

- You need to create a website based on website.jpg image
- Use Bootstrap
- Use Google font: Montserrat
- CSS animated clouds (make the clouds move from left to right)
- When field error is visible, return to normal state on field focus

# JS requirements: #

- Use noUiSlider plugin (+/- buttons are optional)
- Slider #1 Loan amount, increment by 10.000, from 10k-100k
- Slider #2 Loan term, available options: 12, 18, 24, 36 months
- Additional condition for sliders: If loanAmount > 50k then minimum loanTerm >= 24 months

# Form validation: #

- Full name: At least two different words
- Phone number: valid LT number
- Email: valid email
